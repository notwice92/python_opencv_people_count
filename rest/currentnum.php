<?php
header('Access-Control-Allow-Origin: *');
include("config.php");
$con = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_connect_error());
$con->set_charset("utf8");

$location_id = $_GET['location_id'];

$query = "SELECT * FROM count_log WHERE location_id = '$location_id' ORDER BY _id DESC LIMIT 25";

$result = $con->query($query) or die(mysql_error());

if ($result) {
	$max0 = NULL;
	$max1 = NULL;
	$max2 = NULL;
	$max3 = NULL;
	$max4 = NULL;
	$index = 0;
	while ($row = $result -> fetch_assoc()) {
		$temp = $row;
		if(is_null($max0) || $temp['count'] > $max0['count'] && index < 5) {
			$max0 = $temp;
		}
		else if(is_null($max1) || $temp['count'] > $max1['count'] && index < 10) {
			$max1 = $temp;
		}
		else if(is_null($max2) || $temp['count'] > $max2['count'] && index < 15) {
			$max2 = $temp;
		}
		else if(is_null($max3) || $temp['count'] > $max3['count'] && index < 20) {
			$max3 = $temp;
		}
		else if(is_null($max4) || $temp['count'] < $max4['count'] && index < 25) {
			$max4 = $temp;
		}

		$index = $index + 1;
	}
	echo json_encode(array('max0' => $max0, 'max1' => $max1, 'max2' => $max2, 'max3' => $max3, 'max4' => $max4));
}else {
	echo "fail";
}
?>
