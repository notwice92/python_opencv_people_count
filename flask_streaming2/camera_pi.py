from __future__ import print_function

import time
import io
import threading

from imutils.object_detection import non_max_suppression
from imutils import paths
import argparse
import imutils
import numpy as np
import cv2

import requests

import socket
import struct
from PIL import Image

#import image

def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags = 0)
    if len(rects) == 0:
        r = requests.get("http://125.209.199.219/rest/count.php?location_id=2&count=0")
        print (r.status_code)
        return []
    rects[:,2:] += rects[:,:2]
    return rects

def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)

people_cascade = cv2.CascadeClassifier('cascade36.xml')
fgbg = cv2.BackgroundSubtractorMOG2(10, 20, True)

class Camera(object):
    thread = None  # background thread that reads frames from camera
    frame = None  # current frame is stored here by background thread
    originFrame = None
    last_access = 0  # time of last client access to the camera

    def initialize(self):
        if Camera.thread is None:
            print('thread will start')
            # start background frame thread
            Camera.thread = threading.Thread(target=self._thread)
            Camera.thread.start()

            # wait until frames start to be available
        while self.frame is None:
            time.sleep(0)


    def get_frame(self):
        Camera.last_access = time.time()
        self.initialize()
        return self.frame

    def get_origin_frame(self):
        return self.originFrame

    @classmethod
    def _thread(cls):
        print('socket init')
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(('0.0.0.0', 6000))
        server_socket.listen(10)
        connection = server_socket.accept()[0].makefile('rb')
        try:
            while True:
                print('connected')
                # Read the length of the image as a 32-bit unsigned int. If the
                # length is zero, quit the loop
                image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
                if not image_len:
                    break
                # Construct a stream to hold the image data and read the image
                # data from the connection
                image_stream = io.BytesIO()
                image_stream.write(connection.read(image_len))
                # Rewind the stream, open it as an image with PIL and do some
                # processing on it
                image_stream.seek(0)
                cls.originFrame = image_stream.read()

                nparr = np.fromstring(cls.originFrame, np.uint8)
                img = cv2.imdecode(nparr, cv2.CV_LOAD_IMAGE_COLOR)
                fgmask = fgbg.apply(img, learningRate = 0.002)

                rects = detect(fgmask, people_cascade)

                for(x,y,w,h) in rects:
                    count = 0
                    #draw_rects(frame, rects, (255, 255, 255))
                    rects = np.array([[x, y, x + 45, y + 90] for (x, y, w, h) in rects])
                    #rects = np.array([[x, y,  w,  h] for (x, y, w, h) in rects])
                    #print(w)
                    pick = non_max_suppression(rects, probs=None, overlapThresh=0.00001)
                    for (xA, yA, xB, yB) in pick:
                        #cv2.rectangle(fgmask, (xA, yA), (xB, yB), (255, 255, 255), 2)
                        cv2.rectangle(fgmask, (xA, yA), (xB, yB), (255, 255, 255), 2)
                        count = count+1
                    print (count)
                    r = requests.get("http://125.209.199.219/rest/count.php?location_id=2&count="+str(count))
                    print (r.status_code)

                img_str = cv2.imencode('.jpg', fgmask)[1].tostring()
                cls.frame = img_str

                # reset stream for next frame
                image_stream.seek(0)
                image_stream.truncate()

            cls.thread = None
        finally:
            connection.close()
            server_socket.close()
            print('socket closed')


